from scripts.chat import Chat

author = 'Piotr Bogus'
gitlab = 'gitlab.com/AnimusPL/simpleftpchat'
program = 'SimpleFTPChat'
version = 'v0.4'

class WelcomeScreen:
    def __init__(self):
        print('        ___  _____  ___\n'
              ' ___   / __\/__   \/ _ \___\n'
              '/ __| / _\    / /\/ /_)/ __|\n'
              '\__ \/ /     / / / ___/ (__\n'
              '|___/\/      \/  \/    \___|\n'
              f'\nWelcome to {program} {version} by {author}!\n\n')

if __name__ == '__main__':
    WelcomeScreen()
    Chat()