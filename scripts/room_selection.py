def roomSelection():
    print('-------------------------------------------------------\n'
          'Choose a room:   1. main    2. random    3. private\n'
          '-------------------------------------------------------\n'
          'The rooms are encrypted via AES.\n'
          '-------------------------------------------------------\n')
    room = str(input('Selection: '))

    if room == '1':
        room = str('main.log')
        room_password = 'default'

    elif room == '2':
        room = str('random.log')
        room_password = 'default'

    elif room == '3':
        room = str('private.log')
        room_password = str(input('PASSWORD: '))

    else:
        print('------------------------------------------------\n'
        'Invalid selection! Try again!')
        roomSelection()
    return room, room_password