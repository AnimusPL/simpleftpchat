from os import remove, mkdir
from time import sleep
from shutil import rmtree
from scripts.date_and_time import Dateandtime
from scripts.ftp_connection import FTPConnection
from scripts.console_clear import clear
from scripts.room_selection import roomSelection
from scripts.aes_decryption import AESCipher

class Chat:
    def __init__(self):
        filename, room_password = roomSelection()
        aesCipher = AESCipher(room_password)

        print('\n------------------------------------------------'
              '\nPlease insert your login:\n'
              '------------------------------------------------\n')
        # type your login name
        chat_login = input('LOGIN: ')
        while chat_login == '' or ' ' in chat_login:
            print('Your name cannot be empty or contains any spaces.')
            chat_login = input('LOGIN: ')

        # make a connection with ftp server
        ftpConnection = FTPConnection()
        ftpConnection.set()
        try:
            mkdir('temp')
        except:
            pass

        #create a new object contains reading and writting messages
        messagesWindow = MessagesWindow()
        messagesWindow.loadNewMessages(filename, ftpConnection, aesCipher)
        messagesWindow.writeYourMessage(filename, ftpConnection, messagesWindow, chat_login, aesCipher)

class MessagesWindow:
    def __init__(self):
        pass

    def loadNewMessages(self, filename, ftpConnection, aesCipher):
        try:
            while True:
                localfile = open('temp/'+ filename, 'wb')
                ftpConnection.ftp.retrbinary('RETR ' + filename, localfile.write, 1024)
                #ftpConnection.ftp.quit()
                localfile.close()
                clear()
                print('------------------------------------------------------------------------------')
                print('SimpleFTPChat')
                print('Room:', filename)
                print('------------------------------------------------------------------------------')
                for line in open('temp/' + filename):
                    # decryption
                    line = (line.rstrip('\n'))
                    line = aesCipher.decrypt(line).decode('utf-8')
                    print(line)
                print('------------------------------------------------------------------------------')
                print('To write press CTRL + C')
                print('------------------------------------------------------------------------------')
                sleep(1)
        except KeyboardInterrupt:
            print('------------------------------------------------------------------------------')
            print('Avaliable commands:  .q - quit  .r - refresh  .b - back to the room selection')
            print('------------------------------------------------------------------------------')
        except ValueError:
            input('Password was incorrect! To try again press ENTER')
            Chat()

    def writeYourMessage(self, filename, ftpConnection, messagesWindow, chat_login, aesCipher):

        self.your_message = str(input(chat_login + ': '))

        if self.your_message == '':
            input('Message cannot be empty.')
        
        elif self.your_message == '.q':
            rmtree('temp')
            ftpConnection.ftp.quit()
            exit()

        elif self.your_message == '.r':
            pass

        elif self.your_message == '.b':
            Chat()

        else:
            self.actual_date_and_time = Dateandtime().showDateAndTime()

            self.your_message = '[' + self.actual_date_and_time + ']' + ' ' + \
                                '[' + chat_login + ']' + ': ' + self.your_message

            # encryption
            self.your_message = self.your_message
            self.crypted_message = aesCipher.encrypt(self.your_message).decode('utf-8')

            file_to_save = open('temp/' + filename, 'a+')
            file_to_save.write(self.crypted_message + '\n')
            file_to_save.close()
            ftpConnection.ftp.storbinary('STOR ' + filename, open('temp/' + filename, 'rb'))

        messagesWindow.loadNewMessages(filename, ftpConnection, aesCipher)
        messagesWindow.writeYourMessage(filename, ftpConnection, messagesWindow, chat_login, aesCipher)