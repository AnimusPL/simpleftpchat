## **Welcome to simpleFTPchat!**

That was supposed to be a simple chat using FTP protocol to send and receive a messages, but I'm still doing improvements.

Room files are stored on a FTP server. The program is using simple scripts to download file, show text from the file, write new line with text and send them again to the server.

Every room is encrypted via AES, so there is no possibility for now to decrypt messages without password.
That was a major goal for me. 

**To run:**

- python 3 installed
- pip install pycryptodome
- run main.py (f.e. via command 'python3 main.py')


**To do:**

- room adding via console (a room that has not been used for a specified period of time will be automatically deleted).

- chat colors (not important for now)
